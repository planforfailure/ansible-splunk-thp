# Disable Transparent Huge Pages 

[THP](https://docs.splunk.com/Documentation/Splunk/8.2.3/ReleaseNotes/SplunkandTHP) is a known issue affecting older versions of the Linux kernel when running Splunk. To remediate the problem we can use ansible to deploy the process seemlessly to mitigate any future degradation issues by disabling THP.


### Components

As per the Red Hat [solution](https://access.redhat.com/solutions/1320153), the component parts are run sequentionally when calling [deploy_thp.yml](deploy_thp.yml) 

 - [grub_thp_add.yml](tasks/grub_thp_add.yml) - Backup `/etc/default/grub` & `/boot/grub2/grub.cfg`
 - [grub_mkconfig.yml](tasks/grub_mkconfig.yml) - Create new grub2 config
 - [changelog.yml](tasks/changelog.yml) - Update changelog
 - [reboot.yml](tasks/reboot.yml) - Reboot host 
 - [startup.yml](tasks/startup.yml) - Ensure Splunkd.service is started at boot


#### Installation

The role requires escalated privileges on the target hosts:-  

 `$ /usr/bin/ansible-playbook -i inventory deploy_thp.yml -b -K`


#### Summary
Upon successful completion and with no failures in the callback output, you can run the following to confirm transparent huge pages has been disabled and is persistant across reboots.

`$ ansible -i inventory all -a "grep never /proc/cmdline"`
